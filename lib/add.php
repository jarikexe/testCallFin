<?php
require("../conf.php");
$conn = new mysqli($servername, $username, $password, $dbname);

if ($conn->connect_error) {
    echo "No sach database";
    exit;
} 


$stmt = $conn->prepare("INSERT INTO books (title, author, publishing) VALUES (?, ?, ?)");
$stmt->bind_param("sss", $tit, $auth, $pub);



$tit = htmlspecialchars($_POST["title"]);
$auth = htmlspecialchars($_POST["author"]);
$pub = htmlspecialchars($_POST["pub"]);
$stmt->execute();
echo "Book was added";

$stmt->close();
$conn->close();