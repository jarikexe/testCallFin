<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Test</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
	<link rel="stylesheet" href="style.css" crossorigin="anonymous">


	<script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>

	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
</head>

<?php 
	require("conf.php");

	$conn = new mysqli($servername, $username, $password, $dbname);

	if ($conn->connect_error) {
	    die("Connection failed: " . $conn->connect_error);
	} 

	$sql = "SELECT id,title,author,publishing,img_url FROM books";
	$result = $conn->query($sql);

	$conn->close();
 ?>

<body>


	<div class="wrp">

		<h1 class="text-center">Book catalog</h1>
		<div class="row header">
			<div class="col-1">id</div>	
			<div class="col-2">Cover</div>	

			<div class="col-2">author</div>	
			<div class="col-3">title</div>
			<div class="col-3">publisher</div>	
		</div>
		<?php  if ($result->num_rows > 0) : ?>
		<?php while($row = $result->fetch_assoc()) :  ?>
		<div class="row item">
			
			<div class="col"> #  <?php echo $row["id"]; ?></div>	
			<div class="col-2 cover">
				<?php if ($row["img_url"]): ?>
					<img src="<?php echo  $row["img_url"] ?>" class="col-12" alt="Picture of img">
				<?php else: ?>
					<form action="lib/addImg.php" enctype="multipart/form-data" method="POST">
						<input type="file" name="cover">
						<input type="text" hidden="" value="<?php echo $row["id"] ?>" name="id">
						<input type="submit" value="set picture">
						<input type="hidden" name="MAX_FILE_SIZE" value="50000" />
					</form>
				<?php endif ?>
			</div>
			<div class="col-2"><?php echo $row["author"]; ?></div>	
			<div class="col-3"><?php echo $row["title"]; ?></div>
			<div class="col-3"><?php echo $row["publishing"]; ?></div>	
			<div class="col">
				<button type="button" data-id="<?php echo $row["id"]; ?>" class="delete btn btn-default" aria-label="Left Align">
					delete
				</button>
			</div>
			

		</div>
		<?php endwhile ?>
		<?php else: 
			echo "<h3> No books yet</h3>";
		?>
		<?php endif ?>
	
	<!-- Trigger the modal with a button -->
<button type="button" class="addBtn col-12 btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Add book + </button>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add book</h4>
      </div>
      <div class="modal-body">
        <form>
        	<div>
	        	<label for="author">Author</label>
	        	<input type="text" id="author">
			</div>
        	<div>
        		<label for="title">Title</label>
        		<input type="text" id="title">
			</div>

        	<div>
	        	<label for="pub">Publisher</label>
    	    	<input type="text" id="pub">
    	    </div>
        </form>
      </div>
      <div class="modal-footer">
      	<button type="button" class="btn btn-success" id="addBook">Add</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
</div>





<script>
	
	//delite book part
	$(".delete").click(function(){
		var id = $(this).attr("data-id");
		var thisNod = $(this).closest(".item");
		$.ajax({  
		    type: 'POST',  
		    url: '/lib/delete.php', 
		    data: { "id_of_post" : id },
		    success: function(response) {
		    	if(response == "The book was removed"){
		    		thisNod.remove();
		    	}else{
		    		alert(response);
		    	}
		    }
		});
	});

	//add book part
	$("#addBook").click(function(){
		var author = $("#author").val();
		var title = $("#title").val();
		var pub = $("#pub").val();

		if(typeof author == "string" && typeof title == "string" && typeof pub == "string" && author !== "" && title !== "" && title !== ""){
			$.ajax({  
			    type: 'POST',  
			    url: '/lib/add.php', 
			    data: {
			    	"title" : title,
			    	"author" : author,
			    	"pub": pub
			     },
			    success: function(response) {
			    	 location.reload();
			    }
			});
		
		}else{
			alert("Fill all fields");
		}
	});

</script>
	
</body>
</html>